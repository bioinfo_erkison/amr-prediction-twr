FROM continuumio/miniconda3
LABEL authors="Erkison Odih, Ayorinde Afolayan, Anthony Underwood, Varun Shamanna" \
      description="Docker image containing all requirements for hamronization"
RUN apt update


## Install hamronization via conda
COPY hamronization.yml /
RUN conda env create -f /hamronization.yml && conda clean -a
ENV PATH /opt/conda/envs/hamronization/bin:$PATH


# install procps which is required by Nextflow trace
RUN apt install -y procps

