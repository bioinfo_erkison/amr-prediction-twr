process combine_ariba_summaries{
    tag {'combine_ariba_summaries'}
    publishDir "${params.output_dir}/ariba", mode: 'copy'

    input:
    file(summary_files)

    output:
    path 'combined_summary.csv'

    script:
    """
    # combine headers
    echo "\$(head -1 ${summary_files[0]}),known_variants,\$(head -1 ${summary_files[1]} | cut -d',' -f2-)" > combined_summary.csv
    # remove headers and sort
    mkdir sorted_summaries
    tail -n +2 ${summary_files[0]} | sort > sorted_summaries/${summary_files[0]}
    # add blank column to 2nd file to add in known_variants column seperator
    tail -n +2 ${summary_files[1]} | sort | awk -F, '\$1=FS\$1' OFS=, > sorted_summaries/${summary_files[1]}
    # join sorted body text
    join -t , -2 2 sorted_summaries/${summary_files[0]} sorted_summaries/${summary_files[1]} >> combined_summary.csv
    """
}

// RUN HAMRONIZATION 

process run_hamronization_ariba{
    tag {'run_hamronization_ariba'}
    publishDir "${params.output_dir}/hamronization", mode: 'copy'

    input:
    tuple sample_id, file(report_file) 

    output:
    path "${sample_id}.tsv"

    script:
    """
    hamronize ariba --analysis_software_version 1 --reference_database_version 1 --reference_database_id ncbi --input_file_name ${sample_id} --format tsv --output ${sample_id}.tsv ${report_file} 
    """
}

process run_hamronization_summary{
    tag {'run_hamronization_summary'}
    publishDir "${params.output_dir}/hamronization", mode: 'copy'

    input:
    file(hamronized_files) 

    output:
    path 'hamronize_summary.tsv'

    script:
    """
    # summarize the outputs into one file
    hamronize summarize ${hamronized_files} -t tsv -o hamronize_summary.tsv
    """
}